const int buz = 5;//40

void setup() {
  pinMode(buz, OUTPUT);
}

void loop() {
  int i = 0;
  while(i < 2000) {
    tone(buz, i);
    i++;
  }
  noTone(5);
  delay(300);
}
