/*
     B
    ===
  ||   ||
C ||   || A
  || D ||
    ===
  ||   ||
E ||   || G
  ||   ||
    ===      _
     F      | | H
             ¨
*/
const int A = 28;
const int B = 29;
const int C = 30;
const int D = 31;
const int E = 32;
const int F = 33;
const int G = 34;
const int H = 35;

void setup() {
  pinMode(A, OUTPUT);
  pinMode(B, OUTPUT);
  pinMode(C, OUTPUT);
  pinMode(D, OUTPUT);
  pinMode(E, OUTPUT);
  pinMode(F, OUTPUT);
  pinMode(G, OUTPUT);
  pinMode(H, OUTPUT);
}

void clean(){
  digitalWrite(A,LOW);
  digitalWrite(B,LOW);
  digitalWrite(C,LOW);
  digitalWrite(D,LOW);
  digitalWrite(E,LOW);
  digitalWrite(F,LOW);
  digitalWrite(G,LOW);
  digitalWrite(H,LOW);
}

void loop() {
  digitalWrite(A,HIGH);
  delay(500);
  digitalWrite(B,HIGH);
  delay(500);
  digitalWrite(C,HIGH);
  delay(500);
  digitalWrite(D,HIGH);
  delay(500);
  digitalWrite(E,HIGH);
  delay(500);
  digitalWrite(F,HIGH);
  delay(500);
  digitalWrite(G,HIGH);
  delay(500);
  digitalWrite(H,HIGH);

  delay(500);

  clean();

  delay(500);
}
