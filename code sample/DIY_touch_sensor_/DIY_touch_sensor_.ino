const int pin = 3;

void setup() {
  pinMode(pin,INPUT);
  Serial.begin(9600);
}

void loop() {
  int val = digitalRead(pin);
  Serial.println(val);
  delay(50);
}
