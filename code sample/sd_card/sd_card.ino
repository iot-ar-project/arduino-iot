/*
  SD card read/write

  This example shows how to read and write data to and from an SD card file
  The circuit:
   SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)

  created   Nov 2010
  by David A. Mellis
  modified 9 Apr 2012
  by Tom Igoe

  This example code is in the public domain.

*/

#include <SPI.h>
#include <SD.h>
#include <ArduinoJson.h>
#include <string.h>

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.print("Initializing SD card...");

  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    while (1);
  }
  Serial.println("initialization done.");

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File file = SD.open("config.txt");
//  while (file.available()) {
 // Serial.print((char)file.read());
//}
  StaticJsonDocument<2048> doc;
  
  DeserializationError error = deserializeJson(doc, file);

  Serial.println(error.c_str());
if (error) {
  Serial.println(F("Failed to read file, using default configuration"));
} else {
  for (int i = 0; i < doc["trashes"].size(); i++) {
    Serial.println(doc["trashes"][i]["id"].as<int>());
    for (int j = 0; j < doc["trashes"][i]["sensors"].size(); j++) {
      Serial.println((char *)doc["trashes"][i]["sensors"][j]["name"]);
      for (int k = 0; k < doc["trashes"][i]["sensors"][j]["pins"].size(); k++) {
        Serial.println((int)doc["trashes"][i]["sensors"][j]["pins"][k]);
      }
    }
}
}
  file.close();


}

void loop() {
  // nothing happens after setup
}
