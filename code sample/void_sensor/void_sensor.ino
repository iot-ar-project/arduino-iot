const int void_D = 38;

void setup() {
  Serial.begin(9600);
  pinMode(void_D, INPUT);
}
// max refresh 60 ms
// distance règlé a 4 cm
void loop() {
  bool tmp = digitalRead(void_D);

  tmp = !tmp;
  
  Serial.println(tmp);
  delay(60);
}
