#include "LedControl.h"

int DIN = 12;
int CS =  11;
int CLK = 10;

byte b[8]={0x60,0x50,0x48,0x48,0x70,0x48,0x44,0x78,};
byte i[8]={0x10,0x00,0x10,0x10,0x10,0x10,0x10,0x10,};
byte t[8]={0xFF,0xFF,0x18,0x18,0x18,0x18,0x18,0x18,};
byte e[8]={0x3C,0x20,0x20,0x38,0x38,0x20,0x20,0x3C,};
LedControl lc=LedControl(DIN,CLK,CS,0);

void setup(){
 lc.shutdown(0,false);       //The MAX72XX is in power-saving mode on startup
 lc.setIntensity(0,1);      // Set the brightness to maximum value 0 => 15
 lc.clearDisplay(0);         // and clear the display
}

void loop(){ 
 
    printEduc8s();

}

void printEduc8s()
{

  printByte(b);
  delay(1000);
    printByte(i);
  delay(1000);
    printByte(t);
  delay(1000);
    printByte(e);
  delay(1000);
}

void printByte(byte character [])
{
  int i = 0;
  for(i=0;i<8;i++)
  {
    lc.setRow(0,i,character[i]);
  }
}
