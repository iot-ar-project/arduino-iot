const int tilt_D = 4;

void setup() {
  Serial.begin(9600);
  pinMode(tilt_D, INPUT);
}
// max refresh 60 ms

void loop() {
  bool tmp = digitalRead(tilt_D);
  
  Serial.println(tmp);
  delay(60);
}
