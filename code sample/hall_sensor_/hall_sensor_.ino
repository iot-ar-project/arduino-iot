const int hall_d = 26;

void setup() {
  Serial.begin(9600);
  pinMode(hall_d, INPUT);
  
}

void loop() {
  int tmp = 0;
   // lecture analogique possible mais inutile dans le cas actuel
  tmp =  digitalRead(hall_d);
  Serial.println(tmp);
  delay(50);
}
