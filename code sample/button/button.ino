const int pin = 39;

void setup() {
  pinMode(pin, INPUT);
  Serial.begin(9600);
}

void loop() {
  Serial.println(digitalRead(pin));
  delay(60);
}
