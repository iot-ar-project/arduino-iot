const int led_0 = 41;
const int led_1 = 42;
const int led_2 = 43;
const int led_3 = 44;

void setup() {
  pinMode(led_0, OUTPUT);
  pinMode(led_1, OUTPUT);
  pinMode(led_2, OUTPUT);
  pinMode(led_3, OUTPUT);
}

void clean(){
  digitalWrite(led_0, LOW);
  digitalWrite(led_1, LOW);
  digitalWrite(led_2, LOW);
  digitalWrite(led_3, LOW);
}

void loop() {
  digitalWrite(led_0, HIGH);
  delay(300);
  digitalWrite(led_1, HIGH);
  delay(300);
  digitalWrite(led_2, HIGH);
  delay(300);
  digitalWrite(led_3, HIGH);
  delay(300);
  clean();
}
