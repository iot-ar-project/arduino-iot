const int metal_D = 37;

void setup() {
  Serial.begin(9600);
  pinMode(metal_D, INPUT);
}
// max refresh 60 ms
void loop() {
  int tmp = digitalRead(metal_D);

  Serial.println(tmp);
  delay(60);
}
