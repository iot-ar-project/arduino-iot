const int trig = 6;
const int echo = 7;

void setup() {
  pinMode(trig, OUTPUT);
  pinMode(echo, INPUT);
  Serial.begin(9600);
}
 // output en cm
void loop() {
  long duration = 0;
  int distance = 0;
  
  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);

  duration = pulseIn(echo, HIGH);
  distance = duration * 0.034/2;

  Serial.println(distance);
}
