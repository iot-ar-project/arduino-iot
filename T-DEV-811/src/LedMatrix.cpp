#include "LedMatrix.hpp"

#define MAX_LIST_ELEMENT 11

LedMatrix::LedMatrix(int din, int clk, int cd) {
  int i = 0;

  for (auto &subscriber : this->subscribers) {
    eventManager.subscribe(subscriber);
  }
  this->max_el = MAX_LIST_ELEMENT;
  this->lc = new LedControl(din, clk, cd, 0);
  this->fill_byte(this->blank, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
  this->fill_byte(this->begin, 0xFF, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0xFF);
  this->fill_byte(this->end, 0x81, 0x42, 0x24, 0x18, 0x18, 0x24, 0x42, 0x81);
  while (i < 11) {
    this->copy_byte(this->msg_list[i], this->blank);
    this->copy_byte(this->new_msg_list[i], this->blank);
    i++;
  }
  this->copy_byte(this->msg_list[0], this->begin);
  this->copy_byte(this->msg_list[this->max_el - 1], this->end);
  this->copy_byte(this->new_msg_list[0], this->begin);
  this->copy_byte(this->new_msg_list[this->max_el - 1], this->end);
  this->setup();
}

void LedMatrix::setup() {
  this->lc->shutdown(0, false);
  this->lc->setIntensity(0, 1);
  this->lc->clearDisplay(0);
  PT_INIT(&this->pt1);
}

void LedMatrix::fill_byte(byte *target, byte a, byte b, byte c, byte d, byte e, byte f, byte g, byte h) {
  target[0] = a;
  target[1] = b;
  target[2] = c;
  target[3] = d;
  target[4] = e;
  target[5] = f;
  target[6] = g;
  target[7] = h;
}

void LedMatrix::copy_byte(byte *dest, const byte *source) {
  int i = 0;

  while (i < 8) {
    dest[i] = source[i];
    i++;
  }
}

bool LedMatrix::compare_byte(const byte *bite1, const byte *bite2) {
  int i = 0;

  while (i < 8) {
    if (bite1[i] != bite2[i])
      return false;
    i++;
  }
  return true;
}

void LedMatrix::print_byte(byte *character) {
  int i = 0;

  for (i = 0; i < 8; i++) {
    this->lc->setRow(0, i, character[i]);
  }
}

void LedMatrix::swap_list() {
  int i = 1;

  while (i < this->max_el - 1) {
    if (!compare_byte(this->msg_list[i], this->new_msg_list[i])) {
      this->copy_byte(this->msg_list[i], this->new_msg_list[i]);
    } else {
      this->copy_byte(this->msg_list[i], this->blank);
      this->copy_byte(this->new_msg_list[i], this->blank);
    }
    i++;
  }
}

bool LedMatrix::compare_list() {
  int i = 0;

  while (i < this->max_el) {
    if (this->compare_byte(this->msg_list[i], this->new_msg_list[i]))
      i++;
    else
      return false;
  }
  return true;
}

void LedMatrix::set_lid_status(int trash_nbr, bool status) {
  byte op1[8] = {0x20,0x60,0xA6,0x29,0x29,0x2F,0x29,0x29};
  byte cp1[8] = {0x20,0x66,0xA9,0x29,0x2E,0x29,0x29,0x2E};
  byte op2[8] = {0x00,0x00,0xE6,0x29,0x2F,0xE9,0x89,0xE9};
  byte cp2[8] = {0x00,0x00,0xE0,0x2E,0x29,0xEE,0x89,0xEE};
  byte op3[8] = {0x00,0xE0,0x20,0x26,0xE9,0x2F,0x29,0xE9};
  byte cp3[8] = {0x00,0xE0,0x20,0x26,0xE9,0x2E,0x29,0xEE};

  if (trash_nbr == 0) {
    if (status)
      this->copy_byte(this->new_msg_list[1], &op1[0]);
    else
      this->copy_byte(this->new_msg_list[1], &cp1[0]);
  }
  if (trash_nbr == 1) {
    if (status)
      this->copy_byte(this->new_msg_list[4], &op2[0]);
    else
      this->copy_byte(this->new_msg_list[4], &cp2[0]);
  }
  if (trash_nbr == 2) {
    if (status)
      this->copy_byte(this->new_msg_list[7], &op3[0]);
    else
      this->copy_byte(this->new_msg_list[7], &cp3[0]);
  }
}

void LedMatrix::set_filling_status(int trash_nbr, bool status) {
  byte fp1[8] = {0x20,0x60,0xA0,0x2F,0x28,0x28,0x28,0x2F};
  byte ep1[8] = {0x20,0x60,0xA0,0x20,0x2E,0x29,0x29,0x2E};
  byte fp2[8] = {0x00,0x00,0xE0,0x2F,0x28,0xE8,0x88,0xEF};
  byte ep2[8] = {0x00,0x00,0xE0,0x2E,0x29,0xE9,0x89,0xEE};
  byte fp3[8] = {0x00,0xE0,0x20,0x2F,0xE8,0x28,0x28,0xEF};
  byte ep3[8] = {0x00,0xE0,0x20,0x2E,0xE9,0x29,0x29,0xEE};

  if (trash_nbr == 0) {
    if (status)
      this->copy_byte(this->new_msg_list[2], &fp1[0]);
    else
      this->copy_byte(this->new_msg_list[2], &ep1[0]);
  }
  if (trash_nbr == 1) {
    if (status)
      this->copy_byte(this->new_msg_list[5], &fp2[0]);
    else
      this->copy_byte(this->new_msg_list[5], &ep2[0]);
  }
  if (trash_nbr == 2) {
    if (status)
      this->copy_byte(this->new_msg_list[8], &fp3[0]);
    else
      this->copy_byte(this->new_msg_list[8], &ep3[0]);
  }
}

void LedMatrix::set_tile_status(int trash_nbr, bool status) {
  byte tp1[8] = {0x20,0x6F,0xA8,0x28,0x2E,0x28,0x28,0x2F};
  byte np1[8] = {0x20,0x6F,0xA8,0x28,0x2F,0x28,0x28,0x28};
  byte tp2[8] = {0x00,0x0F,0xE8,0x28,0x2E,0xE8,0x88,0xEF};
  byte np2[8] = {0x00,0x0F,0xE8,0x28,0x2F,0xE8,0x88,0xE8};
  byte tp3[8] = {0x00,0xEF,0x28,0x28,0xEE,0x28,0x28,0xEF};
  byte np3[8] = {0x00,0xEF,0x28,0x28,0xEF,0x28,0x28,0xE8};

  if (trash_nbr == 0) {
    if (status)
      this->copy_byte(this->new_msg_list[3], &tp1[0]);
    else
      this->copy_byte(this->new_msg_list[3], &np1[0]);
  }
  if (trash_nbr == 1) {
    if (status)
      this->copy_byte(this->new_msg_list[6], &tp2[0]);
    else
      this->copy_byte(this->new_msg_list[6], &np2[0]);
  }
  if (trash_nbr == 2) {
    if (status)
      this->copy_byte(this->new_msg_list[9], &tp3[0]);
    else
      this->copy_byte(this->new_msg_list[9], &np3[0]);
  }
}

int LedMatrix::display() {
  static unsigned long stamp = millis();
  static int i = 0;
  static unsigned long duree = 2000;

  PT_BEGIN(&this->pt1);
        if (!this->compare_list()) {
          this->swap_list();
          while (!compare_byte(this->msg_list[i], this->end)) {
            if (!compare_byte(this->msg_list[i], this->blank)) {
              this->lc->clearDisplay(0);
              this->print_byte(this->msg_list[i]);
              stamp = millis();
              PT_WAIT_UNTIL(&this->pt1, millis() - stamp > duree);
            }
            i++;
          }
          i = this->max_el - 1;
          this->print_byte(this->msg_list[i]);
          stamp = millis();
          PT_WAIT_UNTIL(&this->pt1, millis() - stamp > duree);
          this->lc->clearDisplay(0);
          i = 0;
        }
  PT_END(&this->pt1);
}

void LedMatrix::execute(Event evt) {
  const String label = String(evt.label);

  if (label.indexOf("filling") > -1) {
    (this->*listeners[(int) Status::Filling])(evt.trashNbr, evt.status);
  } else if (label.indexOf("lid") > -1) {
    (this->*listeners[(int) Status::Lid])(evt.trashNbr, evt.status);
  } else if (label.indexOf("tile") > -1) {
    (this->*listeners[(int) Status::Tile])(evt.trashNbr, evt.status);
  }
}