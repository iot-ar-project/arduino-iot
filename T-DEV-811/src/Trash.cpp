#include "Trash.hpp"

Trash::Trash(int trashNbr) : ITrash(trashNbr), trash_nbr(trashNbr) {
  this->sensors.setStorage(this->tmp_sensors);
}

void Trash::attach(ISensor *sensor) {
  sensor->setup();
  this->sensors.push_back(sensor);
}

void Trash::run() {
  for (unsigned int i = 0; i != this->sensors.size(); i++) {
    this->sensors[i]->listen(this);
    this->sensors[i]->process(this);
  }
}