#include "Config.hpp"

t_config conf = {};

void setup() {
  Serial.begin(9600);

  Config::get_config(&conf);
  for (int index = 0; index < MAX_TRASHES; index++) {
    Serial.print("Trash:  ");
    Serial.println(index);
    for (int index1 = 0; index1 < conf.sensors_size[index]; index1++) {
      Serial.print("Sensor: ");
      Serial.println(index1);
      conf.trashes[index]->attach(conf.sensors[index][index1]);
    }
    Serial.println();
  }
}

void loop() {
  while (true) {
    for (auto &trash : conf.trashes) {
      trash->run();
      conf.matrix->display();
    }
  }
}