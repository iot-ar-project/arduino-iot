#include "sensors/HallSensor.hpp"

HallSensor::HallSensor(int n_pin) {
  this->pin = n_pin;
}

void HallSensor::setup() {
  pinMode(this->pin, INPUT);
}

void HallSensor::process(ITrash *trash) {
}

bool HallSensor::is_open() {
  return digitalRead(this->pin);
}

void HallSensor::listen(ITrash *trash) {
  bool is_open = !this->is_open();

  if (is_open ^ this->last_is_open) {
    // emit event
    trash->cover_open = is_open;
    eventManager.trigger(Event("lid", trash->trash_nbr, is_open));
  }
  this->last_is_open = is_open;
}
