#include "sensors/LightSensor.hpp"

LightSensor::LightSensor(int analog_pin) {
  this->pin = analog_pin;
}

void LightSensor::setup() {
}

void LightSensor::process(ITrash *trash) {
}


bool LightSensor::is_full() {
  long read = analogRead(this->pin);

  read = map(read, 0, 200, 0, 100);
  return (read >= 0 && read <= 30);
}

void LightSensor::listen(ITrash *trash) {
  bool is_full = this->is_full();

  if (is_full^ this->last_is_full &&
      !trash->cover_open) {
    // emit event
    trash->filling_full = is_full;
    eventManager.trigger(Event("filling", trash->trash_nbr, is_full));
  }
  this->last_is_full = (!trash->cover_open) ?  is_full : this->last_is_full;
}