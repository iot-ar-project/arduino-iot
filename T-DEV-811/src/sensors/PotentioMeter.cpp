#include "sensors/PotentioMeter.hpp"

Potentiometer::Potentiometer(int analog_pin) {
  this->pin = analog_pin;
}

void Potentiometer::setup() {
}

void Potentiometer::process(ITrash *trash) {
}


bool Potentiometer::is_open() {
  long read = analogRead(this->pin);

  read = map(read, 0, 1023, 0, 100);
  return !(read >= 0 && read <= 5);
}

void Potentiometer::listen(ITrash *trash) {
  bool is_open = this->is_open();

  if (is_open ^ this->last_is_open) {
    // emit event
    trash->cover_open = is_open;
    eventManager.trigger(Event("lid", trash->trash_nbr, is_open));
  }
  this->last_is_open = is_open;
}