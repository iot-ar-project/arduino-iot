#include "sensors/MetalTouch.hpp"

MetalTouch::MetalTouch(int Pin) {
  this->pin = Pin;
}

void MetalTouch::setup() {
  pinMode(this->pin, INPUT);
}

void MetalTouch::process(ITrash *trash) {
}


bool MetalTouch::is_triggered() {
  return digitalRead(this->pin);
}

void MetalTouch::listen(ITrash *trash) {
  bool is_triggered = !this->is_triggered();

  if (is_triggered ^ this->last_is_triggered) {
    // emit event
    trash->cover_open = is_triggered;
    eventManager.trigger(Event("lid", trash->trash_nbr, is_triggered));
  }
  this->last_is_triggered = is_triggered;
}