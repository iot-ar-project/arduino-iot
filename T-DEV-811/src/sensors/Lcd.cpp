#include "sensors/Lcd.hpp"

Lcd::Lcd(int rs, int enable, int d4, int d5, int d6, int d7) {
  static LiquidCrystal lcd(rs, enable, d4, d5, d6, d7);

  this->lcd = &lcd;
  this->open = false;
  this->tiled = false;
  this->full = false;
}

void Lcd::setup() {
  this->lcd->begin(16, 2);
  this->lcd->print("test");
}

void Lcd::set_full(bool status) {
  this->full = status;
}

void Lcd::set_open(bool status) {
  this->open = status;
}

void Lcd::set_tiled(bool status) {
  this->tiled = status;
}

void Lcd::refresh() {
  this->lcd->clear();
  this->lcd->setCursor(0, 0);
  this->lcd->print("Tiled:");
  this->lcd->setCursor(6, 0);
  if (this->tiled)
    this->lcd->print("YES");
  else
    this->lcd->print("NO");

  this->lcd->setCursor(0, 1);
  this->lcd->print("Open:");
  this->lcd->setCursor(5, 1);
  if (this->open)
    this->lcd->print("YES");
  else
    this->lcd->print("NO");

  this->lcd->setCursor(8, 1);
  this->lcd->print("Full:");
  this->lcd->setCursor(13, 1);
  if (this->full)
    this->lcd->print("YES");
  else
    this->lcd->print("NO");
}

void Lcd::listen(ITrash *trash) {
}

void Lcd::process(ITrash *trash) {
  bool is_refresh = 
      trash->cover_open ^ this->open ||
      trash->filling_full ^ this->full ||
      trash->tiled ^ this->tiled;

  this->set_open(trash->cover_open);
  this->set_full(trash->filling_full);
  this->set_tiled(trash->tiled);
  if (is_refresh) {
    this->refresh();
  }
}
