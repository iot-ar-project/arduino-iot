#include "sensors/Buzzer.hpp"

Buzzer::Buzzer(int pin) {
  this->pin = pin;
}

void Buzzer::setup() {
  pinMode(this->pin, OUTPUT);
  PT_INIT(&this->pt1);
}

int Buzzer::alert() {
  static int i = 0;
  static int duration = 3000;
  static unsigned long last_stamp = millis();
  static unsigned long quick = millis();

  PT_BEGIN(&this->pt1);
        if (this->is_alert) {
          while (i <= duration) {
            tone(this->pin, i++);
            if (millis() - quick > 40) {
              last_stamp = millis();
              PT_WAIT_UNTIL(&this->pt1, millis() - last_stamp > 10);
              quick = millis();
            }
          }
          if (i >= duration) { i = 0; }
          noTone(this->pin);
        }
        PT_WAIT_UNTIL(&this->pt1, millis() - last_stamp > 500);
        last_stamp = millis();
  PT_END(&this->pt1);
}

void Buzzer::turn_on_off(bool value) {
  this->is_alert = value;
  if (!this->is_alert) noTone(this->pin);
}

void Buzzer::listen(ITrash *trash) {
}

void Buzzer::process(ITrash *trash) {
  this->turn_on_off(trash->tiled);
  this->alert();
}
