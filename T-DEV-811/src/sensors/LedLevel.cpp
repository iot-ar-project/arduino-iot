#include "sensors/LedLevel.hpp"

LedLevel::LedLevel(int lvl1, int lvl2, int lvl3, int lvl4) {
  this->led_green = lvl1;
  this->led_yellow = lvl2;
  this->led_yellow2 = lvl3;
  this->led_red = lvl4;
}

void LedLevel::setup() {
  pinMode(this->led_green, OUTPUT);
  pinMode(this->led_yellow, OUTPUT);
  pinMode(this->led_yellow2, OUTPUT);
  pinMode(this->led_red, OUTPUT);

  digitalWrite(led_green, LOW);
  digitalWrite(led_yellow, LOW);
  digitalWrite(led_yellow2, LOW);
  digitalWrite(led_red, LOW);

  PT_INIT(&this->pt1);
}

void LedLevel::process(ITrash *trash) {
  this->blink_on_off(trash->cover_open);
  this->set(trash->filling_full ? 3 : 0);
  this->blink();
}

void LedLevel::clear() {
  digitalWrite(led_green, LOW);
  digitalWrite(led_yellow, LOW);
  digitalWrite(led_yellow2, LOW);
  digitalWrite(led_red, LOW);
}

void LedLevel::display() {
  switch (this->current_level) {
    case 3:
      digitalWrite(led_red, HIGH);
    case 2:
      digitalWrite(led_yellow2, HIGH);
    case 1:
      digitalWrite(led_yellow, HIGH);
    case 0:
      digitalWrite(led_green, HIGH);
      break;
    default:
      break;
  }
}

void LedLevel::set(int level) {
  int lvl = (level > 3) ? 3 : (level < 0) ? 0 : level;

  this->current_level = lvl;
}

void LedLevel::blink_on_off(bool status) {
  this->is_blink = status;
}

int LedLevel::blink() {
  static unsigned long duration = 500;
  static unsigned long last_stamp = millis();

  PT_BEGIN(&this->pt1);
        if (this->is_blink) {
          last_stamp = millis();
          this->clear();
          PT_WAIT_UNTIL(&this->pt1, millis() - last_stamp > duration);
          this->display();
          last_stamp = millis();
          PT_WAIT_UNTIL(&this->pt1, millis() - last_stamp > duration);
        } else {
          this->display();
        }
  PT_END(&this->pt1);
}

void LedLevel::listen(ITrash *trash) {
}