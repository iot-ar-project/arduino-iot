#include "sensors/TiltSensor.hpp"

TiltSensor::TiltSensor(int n_pin) {
  this->pin = n_pin;
}

void TiltSensor::setup() {
  pinMode(this->pin, INPUT);
}

bool TiltSensor::is_tiled() {
  return digitalRead(this->pin) == 0;
}

void TiltSensor::listen(ITrash *trash) {
  bool is_tiled = this->is_tiled();

  if (is_tiled ^ this->last_is_tiled) {
    // emit event
    trash->tiled = is_tiled;
    eventManager.trigger(Event("tile", trash->trash_nbr, is_tiled));
  }
  this->last_is_tiled = is_tiled;
}

void TiltSensor::process(ITrash *) {
}