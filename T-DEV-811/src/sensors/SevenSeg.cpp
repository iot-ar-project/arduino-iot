#include "sensors/SevenSeg.hpp"

SevenSeg::SevenSeg(int A, int B, int C, int D, int E, int F, int G, int H) {
  this->A = A;
  this->B = B;
  this->C = C;
  this->D = D;
  this->E = E;
  this->F = F;
  this->G = G;
  this->H = H;

  this->value = -1;
  this->is_blink = false;
}

void SevenSeg::setup() {
  pinMode(this->A, OUTPUT);
  pinMode(this->B, OUTPUT);
  pinMode(this->C, OUTPUT);
  pinMode(this->D, OUTPUT);
  pinMode(this->E, OUTPUT);
  pinMode(this->F, OUTPUT);
  pinMode(this->G, OUTPUT);
  pinMode(this->H, OUTPUT);
  this->clear();
  PT_INIT(&this->pt1);
}


void SevenSeg::clear() {
  digitalWrite(this->A, LOW);
  digitalWrite(this->B, LOW);
  digitalWrite(this->C, LOW);
  digitalWrite(this->D, LOW);
  digitalWrite(this->E, LOW);
  digitalWrite(this->F, LOW);
  digitalWrite(this->G, LOW);
  digitalWrite(this->H, LOW);
}

void SevenSeg::zero() {
  digitalWrite(this->B, HIGH);
  digitalWrite(this->A, HIGH);
  digitalWrite(this->G, HIGH);
  digitalWrite(this->F, HIGH);
  digitalWrite(this->E, HIGH);
  digitalWrite(this->C, HIGH);
}

void SevenSeg::one() {
  digitalWrite(this->A, HIGH);
  digitalWrite(this->G, HIGH);
}

void SevenSeg::two() {
  digitalWrite(this->B, HIGH);
  digitalWrite(this->A, HIGH);
  digitalWrite(this->D, HIGH);
  digitalWrite(this->E, HIGH);
  digitalWrite(this->F, HIGH);
}

void SevenSeg::three() {
  digitalWrite(this->B, HIGH);
  digitalWrite(this->A, HIGH);
  digitalWrite(this->D, HIGH);
  digitalWrite(this->G, HIGH);
  digitalWrite(this->F, HIGH);
}

void SevenSeg::four() {
  digitalWrite(this->C, HIGH);
  digitalWrite(this->D, HIGH);
  digitalWrite(this->A, HIGH);
  digitalWrite(this->G, HIGH);
}

void SevenSeg::five() {
  digitalWrite(this->B, HIGH);
  digitalWrite(this->C, HIGH);
  digitalWrite(this->D, HIGH);
  digitalWrite(this->G, HIGH);
  digitalWrite(this->F, HIGH);
}

void SevenSeg::six() {
  digitalWrite(this->B, HIGH);
  digitalWrite(this->C, HIGH);
  digitalWrite(this->D, HIGH);
  digitalWrite(this->E, HIGH);
  digitalWrite(this->G, HIGH);
  digitalWrite(this->F, HIGH);
}

void SevenSeg::seven() {
  digitalWrite(this->B, HIGH);
  digitalWrite(this->A, HIGH);
  digitalWrite(this->G, HIGH);
}

void SevenSeg::eight() {
  digitalWrite(this->A, HIGH);
  digitalWrite(this->B, HIGH);
  digitalWrite(this->C, HIGH);
  digitalWrite(this->D, HIGH);
  digitalWrite(this->E, HIGH);
  digitalWrite(this->F, HIGH);
  digitalWrite(this->G, HIGH);
}

void SevenSeg::nine() {
  digitalWrite(this->B, HIGH);
  digitalWrite(this->C, HIGH);
  digitalWrite(this->A, HIGH);
  digitalWrite(this->D, HIGH);
  digitalWrite(this->G, HIGH);
  digitalWrite(this->F, HIGH);
}

void SevenSeg::point() {
  digitalWrite(this->H, HIGH);
}

void SevenSeg::set(int val) {
  this->value = val;
}

void SevenSeg::display() {
  this->clear();

  if (this->value >= 0 && this->value <= 10) {
    if (this->value == 0) {
      this->zero();
    } else if (this->value == 1) {
      this->one();
    } else if (this->value == 2) {
      this->two();
    } else if (this->value == 3) {
      this->three();
    } else if (this->value == 4) {
      this->four();
    } else if (this->value == 5) {
      this->five();
    } else if (this->value == 6) {
      this->six();
    } else if (this->value == 7) {
      this->seven();
    } else if (this->value == 8) {
      this->eight();
    } else if (this->value == 9) {
      this->nine();
    } else if (this->value == 10) {
      this->point();
    }
  }
}

void SevenSeg::set_on_off(bool value) {
  this->is_blink = value;
}

int SevenSeg::blink() {
  static const int duree = 500;
  static unsigned long last_stamp = millis();

  PT_BEGIN(&this->pt1);
        if (this->is_blink) {
          last_stamp = millis();
          this->clear();
          PT_WAIT_UNTIL(&this->pt1, millis() - last_stamp > duree);
          last_stamp = millis();
          this->display();
          PT_WAIT_UNTIL(&this->pt1, millis() - last_stamp > duree);
        } else {
          this->display();
        }
  PT_END(&this->pt1);
}

void SevenSeg::listen(ITrash *trash) {
}

void SevenSeg::process(ITrash *trash) {
  this->set_on_off(trash->cover_open);
  this->set(trash->filling_full ? 1 : 0);
  this->blink();
}
