#include "sensors/Button.hpp"

Button::Button(int n_pin) {
  this->pin = n_pin;
}

void Button::setup() {
  pinMode(this->pin, INPUT_PULLUP);
}

bool Button::is_tipped() {
  return digitalRead(this->pin) == 0;
}

void Button::listen(ITrash *trash) {
  bool is_tipped = this->is_tipped();

  if (is_tipped ^ this->last_is_tipped) {
    // emit event
    trash->tiled = is_tipped;
    eventManager.trigger(Event("tile", trash->trash_nbr, is_tipped));
  }
  this->last_is_tipped = is_tipped;
}

void Button::process(ITrash *) {
}
