#include "sensors/UltrasonicSensor.hpp"

UltrasonicSensor::UltrasonicSensor(int echo_pin, int trigger_pin) {
  this->echo = echo_pin;
  this->trigger = trigger_pin;
  this->full = 0;
}

void UltrasonicSensor::setup() {
  pinMode(this->trigger, OUTPUT);
  pinMode(this->echo, INPUT);
  this->set_full_alert_distance(6);
}

void UltrasonicSensor::process(ITrash *trash) {
}


bool UltrasonicSensor::is_full() {
  long duration = 0;
  int distance = 0;

  digitalWrite(this->trigger, LOW);
  delayMicroseconds(2);
  digitalWrite(this->trigger,HIGH);
  delayMicroseconds(10);

  duration = pulseIn(this->echo, HIGH);
  distance = duration * 0.034 / 2;
  // 0.034 sound speed;
  // distance in cm;
  if (distance < this->full)
      return true;
  return false;
}

void UltrasonicSensor::set_full_alert_distance(int distance) {
  if (distance >= 0 && distance <= 20)
    this->full = distance;
}

void UltrasonicSensor::listen(ITrash *trash) {
  bool is_full = this->is_full();

  if (is_full ^ this->last_is_full &&
     !trash->cover_open) {
    // emit event
    trash->filling_full = is_full;
    eventManager.trigger(Event("filling", trash->trash_nbr, is_full));
  }
  this->last_is_full = (!trash->cover_open) ? is_full : this->last_is_full;
}


