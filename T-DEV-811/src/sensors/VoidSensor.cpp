#include "sensors/VoidSensor.hpp"

VoidSensor::VoidSensor(int n_pin) {
  this->pin = n_pin;
}

void VoidSensor::setup() {
  pinMode(this->pin, INPUT);
}

bool VoidSensor::is_triggered() {
  return digitalRead(this->pin) == 0;
}

void VoidSensor::listen(ITrash *trash) {
  bool is_triggered = this->is_triggered();

  if (is_triggered ^ this->last_is_triggered &&
      !trash->cover_open) {
    // emit event
    trash->filling_full = is_triggered;
    eventManager.trigger(Event("filling", trash->trash_nbr, is_triggered));
  }
  this->last_is_triggered = (!trash->cover_open) ? is_triggered : this->last_is_triggered;
}

void VoidSensor::process(ITrash *) {
}
