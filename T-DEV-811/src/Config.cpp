#include "Config.hpp"

t_config Config::get_default_config() {
 return {
      .trashes = {
          new Trash(0),
          new Trash(1),
          new Trash(2),
      },
      .sensors = {
          {
              new HallSensor(26),
              new UltrasonicSensor(7, 6),
              new TiltSensor(27),
              new Lcd(9, 8, 22, 23, 24, 25),
          },
          {
              new Potentiometer(A13),
              new LightSensor(A12),
              new TiltSensor(36),
              new SevenSeg(28, 29, 30, 31, 32, 33, 34, 35),
              new Buzzer(5),
          },
          {
              new MetalTouch(37),
              new VoidSensor(38),
              new Button(39),
              new LedLevel(41, 42, 43, 44),
              new Buzzer(40),
          },
      },
      .sensors_size = {
        4,
        5,
        5
      }
  };
}

void Config::get_config(t_config *conf) {
  StaticJsonDocument<2046> doc;

  SD.begin(53);
  File file = SD.open("config.txt");
  DeserializationError error = deserializeJson(doc, file);
  if (error) {
    Serial.println(F("Failed to read file:"));
    Serial.println(error.c_str());
    conf = &Config::get_default_config();
  } else {
    const JsonArray pins = doc["led_matrix"]["pins"];

    conf->matrix = new LedMatrix(pins[0], pins[1], pins[2]);
    for (unsigned int i = 0; i < doc["trashes"].size(); i++) {
      conf->trashes[i] = new Trash(doc["trashes"][i]["id"].as<int>());
      conf->sensors_size[i] = doc["trashes"][i]["sensors"].size();
      for (unsigned int j = 0; j < doc["trashes"][i]["sensors"].size(); j++) {
        conf->sensors[i][j] = SensorBuilder::create_instance(doc["trashes"][i]["sensors"][j]["name"],
                                                             doc["trashes"][i]["sensors"][j]["pins"]);
      }
    }
  }
  file.close();
}
