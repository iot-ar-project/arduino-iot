/*
  Event

  This class serves as a "callback" manager to register events
  to happen on certain triggers or after certain intervals.
 */

#include "Event.h"

/**
 * Constructs a new EventManager and
 * figures out the size of the available
 * array slots.
 */
EventManager::EventManager() {
  _subPos = 0;
  _subSize = sizeof(_sub) / sizeof(Subscriber);
}

/**
 * Subscribes a new Subscriber to the
 * event manager.
 */
void EventManager::subscribe(Subscriber sub) {
  if (_subSize >= _subPos) {
    _sub[_subPos] = sub;
    _subPos++;
  }
}

/**
 * Triggers a specified event which will find the applicable
 * Subscriber and execute it's EventTask
 */
void EventManager::trigger(Event evt) {
  for (int i = 0; i < _subSize; i++) {
    Subscriber *sub = &_sub[i];

    if (!strcmp(evt.label, sub->label)) {
      // Execute event
      sub->task->execute(evt);
    }
  }
}

EventManager eventManager = EventManager();