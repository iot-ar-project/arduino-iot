/*
  Event

  This class serves as a "callback" manager to register events
  to happen on certain triggers or after certain intervals.
 */

#ifndef EVENT_H
#define EVENT_H

#include <Arduino.h>

/**
 * Event structure is the basic Event
 * object that can be dispatched by the
 * manager.
 */
struct Event
{
  Event() : label(NULL) {}
  Event(const char *cLabel) : label(cLabel) {}
  Event(const char *cLabel, int cTrashNbr, boolean cStatus) : label(cLabel), trashNbr(cTrashNbr), status(cStatus) {}
  const char *label;
  int trashNbr;
  boolean status;
};

/**
 * EventTask is a structure that serves as an
 * abstract class of a "dispatchable" object.
 */
struct EventTask
{
  virtual void execute(Event evt) = 0;
};

/**
 * The Subscriber is the object that
 * encapsulates the Event it's listening for
 * and the EventTask to be executed.
 */
struct Subscriber
{
  Subscriber() : label(NULL), task(NULL) {}
  Subscriber(const char *cLabel, EventTask *cTask) : label(cLabel), task(cTask) {}

  const char *label;
  EventTask *task;
};

/**
 * The EventManager is responsible for gathering subscribers
 * and dispatching them when the requested Event is
 * triggered.
 */
class EventManager
{
public:
  EventManager();
  void subscribe(Subscriber sub);
  void trigger(Event evt);
private:
  Subscriber _sub[10]; // 10 available subscriber slots
  unsigned int _subSize;
  unsigned int _subPos;
};

extern EventManager eventManager;

#endif