#ifndef I_TRASH_HPP
#define I_TRASH_HPP


class ISensor;

class ITrash {
public:
  ITrash(int trashNbr) : trash_nbr(trashNbr){};

  virtual void attach(ISensor *) {};

  virtual void run() {};

  int trash_nbr = -1;

  bool cover_open = false;
  bool filling_full = false;
  bool tiled = false;
};

#endif /*I_TRASH_HPP*/