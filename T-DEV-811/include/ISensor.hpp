#ifndef I_SENSOR_HPP
#define I_SENSOR_HPP

#include "ITrash.hpp"
#include "LedMatrix.hpp"

class ISensor {
public:
  virtual void setup() {};

  virtual void listen(ITrash *) {};

  virtual void process(ITrash *) {};
};

#endif /*I_SENSOR_HPP*/