#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <Arduino.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <SD.h>
#include "Trash.hpp"
#include "ISensor.hpp"
#include "SensorBuilder.hpp"
#include "LedMatrix.hpp"

#define MAX_TRASHES 3
#define MAX_SENSORS 5

typedef struct s_config {
  ITrash *trashes[MAX_TRASHES];
  ISensor *sensors[MAX_TRASHES][MAX_SENSORS];
  unsigned int sensors_size[3];
  LedMatrix *matrix;
} t_config;

class Config {
public:
  static t_config get_default_config();

  static void get_config(t_config *);
};

#endif