#ifndef LED_MATRIX
#define LED_MATRIX

#include <Arduino.h>
#include "pt.h"
#include "LedControl.h"
#include "Vector.h"
#include "Event.h"

/*
    msg list format 
    p1 open / close 
    p1 remplissage
    p1 tiled
    ...
    ...
    p3 tiled
*/
class LedMatrix : public EventTask {
private:
  enum class Status : int {
    Filling = 0,
    Lid = 1,
    Tile = 2
  };
  Subscriber subscribers[3] = {
      Subscriber("filling", this),
      Subscriber("lid", this),
      Subscriber("tile", this)
  };

  typedef void (LedMatrix::*set_status)(int, bool);

  set_status listeners[3] = {
      &LedMatrix::set_filling_status,
      &LedMatrix::set_lid_status,
      &LedMatrix::set_tile_status
  };

  void execute(Event evt) override;

  LedControl *lc{};
  byte blank[8]{};
  byte begin[8]{};
  byte end[8]{};
  byte msg_list[11][8]{};
  byte new_msg_list[11][8]{};
  int max_el{};

  struct pt pt1{};

  void fill_byte(byte *target, byte a, byte b, byte c, byte d, byte e, byte f, byte g, byte h);

  bool compare_byte(const byte bite1[8], const byte bite2[8]);

  void copy_byte(byte *dest, const byte *source);

  void swap_list();

  bool compare_list();

  void print_byte(byte *character);

public:
  LedMatrix(int din, int clk, int cd);

  void setup();

  int display();

  void set_lid_status(int trash_nbr, bool status); // open true
  void set_filling_status(int trash_nbr, bool status); // full true
  void set_tile_status(int trash_nbr, bool status); // tiled true
};

#endif /*LED_MATRIX*/