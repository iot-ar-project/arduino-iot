#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <Arduino.h>
#include "ISensor.hpp"

class Button : public ISensor {
private:
  int pin;

public:
  explicit Button(int pin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  bool is_tipped();

  bool last_is_tipped = false;
};

#endif