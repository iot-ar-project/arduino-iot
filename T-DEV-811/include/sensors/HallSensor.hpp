#ifndef HALL_SENSOR_HPP
#define HALL_SENSOR_HPP

#include <Arduino.h>
#include "ISensor.hpp"

class HallSensor : public ISensor {
private:
  int pin;

public:
  explicit HallSensor(int n_pin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  bool is_open();

  bool last_is_open = false;
};

#endif /*HALL_SENSOR_HPP*/