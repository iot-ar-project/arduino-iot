#ifndef TILT_SENSOR_HPP
# define TILT_SENSOR_HPP

#include <Arduino.h>
#include "ISensor.hpp"

class TiltSensor : public ISensor {
private:
  int pin;

public:
  explicit TiltSensor(int n_pin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  bool is_tiled();

  bool last_is_tiled = false;
};

#endif /*TILT_SENSOR_HPP*/