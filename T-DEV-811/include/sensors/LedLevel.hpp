#ifndef  LED_LEVELHPP
#define LED_LEVELHPP

#include <Arduino.h>
#include "pt.h"
#include "ISensor.hpp"

class LedLevel : public ISensor {
private :
  int led_green = 0;
  int led_yellow = 0;
  int led_yellow2 = 0;
  int led_red = 0;
  int current_level = 0;

  struct pt pt1;
  bool is_blink = false;

  void display();

public:
  LedLevel(int lvl1, int lvl2, int lvl3, int lvl4);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  void set(int level);

  void clear();

  int blink();

  void blink_on_off(bool status);
};

#endif /*LED_LEVELHPP*/