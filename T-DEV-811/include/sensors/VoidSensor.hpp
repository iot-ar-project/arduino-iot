#ifndef VOID_SENSOR_HPP
#define VOID_SENSOR_HPP

#include<Arduino.h>
#include "ISensor.hpp"

/*  class adapted for digital detection only ON OFF supported
    current sensor calibration : 4cm
    the folowing code asumming the sensor is already calibrated
*/

class VoidSensor : public ISensor {
private:
  int pin;

public:
  explicit VoidSensor(int pin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  bool is_triggered();

  bool last_is_triggered = false;
};

#endif /*VOID_SENSOR_HPP*/