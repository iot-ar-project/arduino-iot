#ifndef LCD_HPP
#define LCD_HPP

#include <Arduino.h>
#include "LiquidCrystal.h"
#include "ISensor.hpp"

class Lcd : public ISensor {
private:
  LiquidCrystal *lcd;
  bool tiled;
  bool open;
  bool full;

public:
  Lcd(int rs, int enable, int d4, int d5, int d6, int d7);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  void set_full(bool status);

  void set_open(bool status);

  void set_tiled(bool status);

  void refresh();
};

#endif /*LCD_HPP*/