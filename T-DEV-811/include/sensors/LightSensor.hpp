#ifndef LIGHT_SENSOR_HPP
#define LIGHT_SENSOR_HPP

#include <Arduino.h>
#include "ISensor.hpp"

class LightSensor : public ISensor {
private:
  int pin;
public:
  explicit LightSensor(int analog_ppin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  bool is_full();

  bool last_is_full= false;
};

#endif /*LIGHT_SENSOR_HPP*/