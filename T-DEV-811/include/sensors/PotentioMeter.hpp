#ifndef POTENTIOMETER_HPP
#define POTENTIOMETER_HPP

#include <Arduino.h>
#include "ISensor.hpp"

class Potentiometer : public ISensor {
private:
  int pin;

public:
  explicit Potentiometer(int analog_ppin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  bool is_open();

  bool last_is_open = false;
};

#endif /*POTENTIOMETER_HPP*/