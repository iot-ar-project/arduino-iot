#ifndef SEVEN_SEG_HPP
#define SEVEN_SEG_HPP

#include <Arduino.h>
#include "pt.h"
#include "ISensor.hpp"

/*
     B
    ===
  ||   ||
C ||   || A
  || D ||
    ===
  ||   ||
E ||   || G
  ||   ||
    ===      _
     F      | | H
             ¨
*/

class SevenSeg : public ISensor {
private:
  int A, B, C, D, E, F, G, H;
  int value;
  struct pt pt1;
  bool is_blink;

  void clear();

  void zero();

  void one();

  void two();

  void three();

  void four();

  void five();

  void six();

  void seven();

  void eight();

  void nine();

  void point();

  void display();

public:
  SevenSeg(int A, int B, int C, int D, int E, int F, int G, int H);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  void set(int val);

  void set_on_off(bool value);

  int blink();
};

#endif /*SEVEN_SEG_HPP*/