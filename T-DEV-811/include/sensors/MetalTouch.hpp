#ifndef METAL_TOUCH_HPP
#define METAL_TOUCH_HPP

#include <Arduino.h>
#include "ISensor.hpp"

class MetalTouch : public ISensor {
private:
  int pin = 0;

public:
  explicit MetalTouch(int pin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  bool is_triggered();

  bool last_is_triggered = false;
};

#endif /*METAL_TOUCH_HPP*/