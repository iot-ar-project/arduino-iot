#ifndef ULTRASONIC_SENSOR_HPP
#define ULTRASONIC_SENSOR_HPP

#include <Arduino.h>
#include "ISensor.hpp"

class UltrasonicSensor : public ISensor {
private:
  int echo;
  int trigger;
  int full;

public:
  UltrasonicSensor(int echo_pin, int trigger_pin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  bool is_full();

  void set_full_alert_distance(int distance);

  bool last_is_full = false;
};

#endif /*ULTRASONIC_SENSOR_HPP*/