#ifndef BUZZER_HPP
#define BUZZER_HPP

#include <Arduino.h>
#include "pt.h"
#include "ISensor.hpp"

class Buzzer : public ISensor {
private:
  int pin = 0;
  struct pt pt1;
  bool is_alert = false;

public:
  explicit Buzzer(int pin);

  void setup() override;

  void listen(ITrash *) override;

  void process(ITrash *) override;

  int alert();

  void turn_on_off(bool value);
};

#endif /*BUZZER_HPP*/