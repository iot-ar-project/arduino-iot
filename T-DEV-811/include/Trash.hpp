#ifndef TRASH_HPP
#define TRASH_HPP

#include <Arduino.h>
#include "Sensors.hpp"
#include "ITrash.hpp"
#include "Vector.h"

class Trash : public ITrash {
private:
  ISensor *tmp_sensors[10];
public:
  Vector<ISensor *> sensors;

  int trash_nbr;
  
  explicit Trash(int trashNbr);

  void attach(ISensor *) override;

  void run() override;

  bool cover_open = false;
  bool filling_full = false;
  bool tiled = false;
};

#endif /*TRASH_HPP*/