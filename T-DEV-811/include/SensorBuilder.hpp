#ifndef SENSOR_BUILDER_HPP
#define SENSOR_BUILDER_HPP

#include "ITrash.hpp"

class SensorBuilder {
public:
  static ISensor *create_instance(const char *class_name, JsonArray params) {
    if (!strcmp(class_name, "Button")) {
      return new Button(params[0].as<int>());
    } else if (!strcmp(class_name, "Buzzer")) {
      return new Buzzer(params[0].as<int>());
    } else if (!strcmp(class_name, "HallSensor")) {
      return new HallSensor(params[0].as<int>());
    } else if (!strcmp(class_name, "Lcd")) {
      return new Lcd(params[0].as<int>(), params[1].as<int>(), params[2].as<int>(), params[3].as<int>(), params[4].as<int>(), params[5].as<int>());
    } else if (!strcmp(class_name, "LedLevel")) {
      return new LedLevel(params[0].as<int>(), params[1].as<int>(), params[2].as<int>(), params[3].as<int>());
    } else if (!strcmp(class_name, "LightSensor")) {
      return new LightSensor(params[0].as<int>());
    } else if (!strcmp(class_name, "MetalTouch")) {
      return new MetalTouch(params[0].as<int>());
    } else if (!strcmp(class_name, "Potentiometer")) {
      return new Potentiometer(params[0].as<int>());
    } else if (!strcmp(class_name, "SevenSeg")) {
      return new SevenSeg(params[0].as<int>(), params[1].as<int>(), params[2].as<int>(), params[3].as<int>(), params[4].as<int>(), params[5].as<int>(), params[6].as<int>(), params[7].as<int>());
    } else if (!strcmp(class_name, "TiltSensor")) {
      return new TiltSensor(params[0].as<int>());
    } else if (!strcmp(class_name, "UltrasonicSensor")) {
      return new UltrasonicSensor(params[0].as<int>(), params[1].as<int>());
    } else if (!strcmp(class_name, "VoidSensor")) {
      return new VoidSensor(params[0].as<int>());
    }
    return nullptr;
  }
};

#endif /*SENSOR_BUILDER_HPP*/