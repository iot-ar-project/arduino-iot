# T-DEV-811 Arduino-IOT

> “One of the myths about the Internet of Things is that companies have all the data they need, but their real challenge is making sense of it. In reality, the cost of collecting some kinds of data remains too high, the quality of the data isn’t always good enough, and it remains difficult to integrate multiple data sources.” — Chris Murphy, Editor, Information Week

## Hardware schematics

For the schematics of the project I (Clement Cazaubon) use the [Fritzing](https://fritzing.org/home/) software. As the software is not free I provide the software binaries in the [schématics](./schématics) directory (windows version)

